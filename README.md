# AlbumPassword

## Description
AlbumPassword is a module for [Gallery 3](http://gallery.menalto.com/) which will allow registered users to assign a password to a public album.  With this module enabled, registered users will be able to assign/remove a password to any album they have edit privileges on by using the Album Options -> Assign password / Album Options -> Remove password menu options. Once a password has been assigned, the album will not appear to guest and other non-admin users.  

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/98856).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.  

Once installed, users will be able to assign a password to any album by selecting Album Options -> Assign password from the menu for the album they wish to protect.  Protected albums can be accessed by clicking "Unlock albums" on the main menu and entering the assigned password.

This module has a configuration screen located at Admin > Settings > Album Password settings.  The following options are available:
 - Do not require passwords -- If checked, protected albums will not show up for guest users, however if the URL to the album is entered into the web browser directly, then it can be accessed without entering in the configured password.  If checked guest users will need to enter the configured password first.

## History
**Version 3.3.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 14 August 2021.
>
> Download: [Version 3.3.0](/uploads/f2aaeda83b86e9c7de73396ebc663e7c/albumpassword330.zip)

**Version 3.2.1:**
> - Bug fix for Gallery 3.0.2.
> - Released 01 June 2011.
>
> Download: [Version 3.2.1](/uploads/67aacc0d8d9ef902832a15ae97a2a9b7/albumpassword321.zip)

**Version 3.2.0:**
> - Removed password case sensitivity.
> - Released 25 April 2011.
>
> Download: [Version 3.2.0](/uploads/82d1999a5ff7c301935249d09c7b5ef9/albumpassword320.zip)

**Version 3.1.0:**
> - Modified "hide only" mode to automatically log the visitor into the album that they're looking at.
> - Released 02 March 2011.
>
> Download: [Version 3.1.0](/uploads/6fc2aabfbc85bd5f1503bb53c9376dd1/albumpassword310.zip)

**Version 3.0.0:**
> - Increased version number to 3
> - Fixed all known bugs between this module and Gallery 3.0.1
> - Re-wrote the hide code to filter out protected items from search results and other dynamic pages.
> - Released 08 February 2011.
>
> Download: [Version 3.0.0](/uploads/fc77da949488a2c933fc6d7745e7287b/albumpassword300.zip)

**Version 2.0.0:**
> - Re-wrote the code to hide the album (and by extension it's thumbnail) instead of the contents of the album.
> - Removed the ability to protect the root gallery album (this is kind of pointless now that the album no longer appears to be empty).
> - Added an admin screen with an option to choose between hiding albums only and denying access to the protected areas.
> - Released 08 November 2010.
>
> Download: [Version 2.0.0](/uploads/f033b2e1d741eb335c2eac15c126575d/albumpassword200.zip)

**Version 1.1.0:**
> - Fixed compatibility issue with older versions of Internet Explorer and Firefox.
> - Released 01 November 2010.
>
> Download: [Version 1.1.0](/uploads/1a97a369021f5ca40cfe50695b93e62b/albumpassword110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 29 October 2010.
>
> Download: [Version 1.0.0](/uploads/d7c76790428f9ddf970cc7f684344ed6/albumpassword100.zip)
